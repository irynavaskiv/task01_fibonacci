package vaskiv;

import java.util.ArrayList;
import java.util.Scanner;

public class fibonacci {
    private static int Fibonacci1;
    private static int Fibonacci2;

    public static void main(String[] args) {
        final int NUMBER_100 = 100;
        Scanner scanner = new Scanner(System.in);
        int leftEdge = getLeftEdge(scanner);
        int rightEdge = getRightEdge(scanner, leftEdge);

        ArrayList<Integer> oddNumbers = getOddNumbers(leftEdge, rightEdge);
        ArrayList<Integer> evenNumbers = getEvenNumbers(leftEdge, rightEdge);

        System.out.println("Sum odd numbers: " + getSumNumbers(oddNumbers));
        System.out.println("Sum even numbers: " + getSumNumbers(evenNumbers));

        oddNumbers = new ArrayList<Integer>();
        evenNumbers = new ArrayList<Integer>();
        oddNumbers.add(Fibonacci1);
        evenNumbers.add(Fibonacci2);
        int countNumbers;
        do {
            System.out.println("Enter count of fibonacci numbers:");
            countNumbers = scanner.nextInt();
        } while (countNumbers < 3);

        for (int i = 2; i < countNumbers; i++) {
            Fibonacci2 += Fibonacci1;
            Fibonacci1 = Fibonacci2 -Fibonacci1;
            if (Fibonacci2 % 2 == 0) {
                evenNumbers.add(Fibonacci2);
            } else {
                oddNumbers.add(Fibonacci2);
            }
        }
        System.out.println("Percent odd numbers on the fibonacci numbers: " + ((((double) oddNumbers.size()) / countNumbers) * NUMBER_100));
        System.out.println("Percent even numbers on the fibonacci numbers: " + (((double) evenNumbers.size()) / countNumbers) * NUMBER_100);
    }

    private static ArrayList<Integer> getEvenNumbers(int leftEdge, int rightEdge) {
    }

    private static int getLeftEdge(final Scanner scanner) {
        System.out.println("Enter left edge: ");
        return scanner.nextInt();
    }

    private static int getRightEdge(final Scanner scanner, final int leftEdge) {
        int rightEdge;
        do {
            System.out.println("Enter right edge: ");
            rightEdge = scanner.nextInt();
        } while (rightEdge < 0 || rightEdge <= leftEdge);
        return rightEdge;
    }

        private static ArrayList<Integer> getOddNumbers(final int leftEdge,
                                                    final int rightEdge) {
        ArrayList<Integer> oddNumbers = new ArrayList<Integer>();
        System.out.println("Odd numbers from the start:");
        for (int i = leftEdge; i <= rightEdge; i++) {
            if (i % 2 != 0) {
                System.out.print(i + " ");
                oddNumbers.add(i);
            }
        }
        return oddNumbers;
    }

     private static Integer getSumNumbers(final ArrayList<Integer> numbers) {
        Integer sumNumbers = null;
        for (Integer i : numbers) {
            if (sumNumbers == null) {
                sumNumbers = i;
            } else {
                sumNumbers += i;
            }
        }
        return sumNumbers;
    }
}